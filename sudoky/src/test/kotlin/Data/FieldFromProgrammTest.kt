package Data

import org.junit.Assert.*
import org.junit.Test
import work.FieldUtils

class FieldFromProgrammTest {

    @Test
    fun testGetField() {
        val field = FieldFromProgramm().getField()
        FieldUtils.print(field)
        assertNotNull(field)
    }

}