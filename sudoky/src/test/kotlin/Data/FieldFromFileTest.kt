package Data

import org.junit.Assert.*
import org.junit.Test
import work.FieldUtils

class FieldFromFileTest {

    @Test
    fun testGetField() {
        val fileField = FieldFromFile("field1.txt").getField()
        FieldUtils.print(fileField)
        assertNotNull(fileField)
    }


}