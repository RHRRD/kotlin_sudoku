package Data;

import Elements.FieldElement;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class FieldFromFile implements Field {

    private String name;

    public FieldFromFile(String name) {
        this.name = name;
    }

    @Nullable
    public FieldElement[][] getField() {
        File file = new File(getClass().getClassLoader().getResource("fields/" + name).getFile());
        FieldElement[][] field;
        try {
            Scanner scanner = new Scanner(file);
            int length = scanner.nextInt();
            field = new FieldElement[length][length];
            while(scanner.hasNext()) {
                for (int i = 0; i < field.length; i++) {
                    for (int j = 0; j < field[i].length; j++) {
                        int num = scanner.nextInt();
                        field[i][j] = num == 0 ? new FieldElement(num, new ArrayList<Integer>(Elements.Field.Companion.getNumbers())) :
                                new FieldElement(num, Collections.<Integer>emptyList());
                    }
                }
            }
            scanner.close();
            return field;
        } catch (FileNotFoundException ex) {
            System.out.println("File with name: \"" + name + "\" not found.");
        } catch (NoSuchElementException ex) {
            System.out.println("Error in file.");
        }
        return null;
    }
}
