package Data;

import Elements.FieldElement;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class FieldFromConsole implements Field {

    @Nullable
    public FieldElement[][] getField() {
        FieldElement[][] field;
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter size of field: ");
            int size = scanner.nextInt();
            field = new FieldElement[size][size];
            for (int i = 0; i < size; i++) {
                System.out.println("Enter line: " + (i + 1));
                for (int j = 0; j < size; j++) {
                    System.out.println("Enter number: " + (j + 1));
                    int num = scanner.nextInt();
                    field[i][j] = num == 0 ? new FieldElement(num, new ArrayList<Integer>(Elements.Field.Companion.getNumbers())) :
                            new FieldElement(num, Collections.<Integer>emptyList());
                }
            }
            System.out.println("Ok. I hope you did everything right...");
            return field;
        } catch (Exception ex) {
            System.out.println("It doesn't work like this...");
            return null;
        }
    }

}
