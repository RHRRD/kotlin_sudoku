package Data

import Elements.FieldElement

interface Field {

    fun getField(): Array<Array<FieldElement>>

}