package Data

import Elements.FieldElement

class FieldFromProgramm : Field {

    companion object {
        private val field = arrayOf<Array<Int>>(
                arrayOf<Int>(8, 0, 0, 0, 0, 0, 0, 0, 0),
                arrayOf<Int>(0, 0, 3, 6, 0, 0, 0, 0, 0),
                arrayOf<Int>(0, 7, 0, 0, 9, 0, 2, 0, 0),
                arrayOf<Int>(0, 5, 0, 0, 0, 7, 0, 0, 0),
                arrayOf<Int>(0, 0, 0, 0, 4, 5, 7, 0, 0),
                arrayOf<Int>(0, 0, 0, 1, 0, 0, 0, 3, 0),
                arrayOf<Int>(0, 0, 1, 0, 0, 0, 0, 6, 8),
                arrayOf<Int>(0, 0, 8, 5, 0, 0, 0, 1, 0),
                arrayOf<Int>(0, 9, 0, 0, 0, 0, 4, 0, 0)
        )
    }

    override fun getField(): Array<Array<FieldElement>> {
        return Elements.Field.createField(field)
    }

}
