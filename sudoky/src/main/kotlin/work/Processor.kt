package work

import Data.FieldFromConsole
import Data.FieldFromFile
import Data.FieldFromProgramm
import Elements.Point

class Processor {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
//            val mainField = Elements.Field(FieldFromProgramm().getField())
//            val mainField = Elements.Field(FieldFromConsole().getField())
            val mainField = Elements.Field(FieldFromFile("field_diff.txt").getField())

            if (mainField == null) {
                println("Oooops... Field is null...")
            }

            print("Input filed:")
            FieldUtils.print(mainField)
            Processor().execute(mainField)
        }
    }

    private fun execute(mainField: Elements.Field) {
        var lengthOfStack = 0
        var y = 0
        var diff = 0
        var field = mainField.clone()
        while (!FieldUtils.finishAll(field) && y < 2000) {
            FieldUtils.evaluateStringsAndColumns(field)
            diff += FieldUtils.evaluateNumbers(field)
            if (!FieldUtils.isCorrect(field)) {
                field = field.pop()
                lengthOfStack--
            }
            if (diff == 0 && FieldUtils.isCorrect(field)) {
                val point = FieldUtils.findPoint(field)
                if (FieldUtils.finishAll(field)) {
                    break
                }
                if (point.number == -1) {
                    field = field.pop()
                    lengthOfStack--
                } else {
                    val newNumber = field.fields[point.x][point.y].listOfNumber.iterator().next()
                    val newOldField = Elements.Field(field.copyFields(), field.prevField, Point(point.x, point.y, newNumber))
                    val newFields = newOldField.copyFields()
                    newFields[point.x][point.y].number = newNumber
                    newFields[point.x][point.y].listOfNumber.remove(newNumber as Any)
                    field = Elements.Field(newFields, newOldField, null)
                    lengthOfStack++
                }
            }
            diff = 0
            y++
        }
        println("Output field:")
        FieldUtils.print(field)
        println("Steps: " + y)
        println("Field is correct: " + FieldUtils.isCorrect(field))
    }

}