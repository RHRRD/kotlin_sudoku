package Elements

import java.util.*

class FieldElement(var number: Int, var listOfNumber: MutableCollection<Int>) {

    companion object {

        fun evaluateFieldElement(fieldElement: FieldElement): FieldElement {
            if (fieldElement.number != 0) {
                return FieldElement(fieldElement.number, Collections.emptyList())
            }
            if (fieldElement.listOfNumber.size == 1) {
                return FieldElement(fieldElement.listOfNumber.elementAt(0), Collections.emptyList())
            }
            return fieldElement
        }

    }
}
